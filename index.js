const sass = require('sass');

const unsupportedStyle = ['nested', 'compact'];

const mapOptions = function(options){

    if(options.outputStyle && unsupportedStyle.includes(options.outputStyle)){
       delete options.outputStyle;
    }

    return options;
}

module.exports = {

    renderSync: function(options){
        return sass.renderSync(mapOptions(options))
    },

    render: function(options, callback){
        return sass.render(mapOptions(options), callback)
    }

}